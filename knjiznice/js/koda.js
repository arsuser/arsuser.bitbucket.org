/* global $, btoa */

var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var stPacienta=2;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function generirajPodatke(stpacienta) {
  
  if(stpacienta==1){
    var ime= "Peter";
    var priimek= "Klepec";
    var systolic= 95;
    var diastolic= 65;
  }
  
  if(stpacienta==2){
    ime= 'Mojca';
    priimek= 'Pokraculja';
    systolic= 132;
    diastolic= 86;
  }
  
  if(stpacienta==3){
    ime= "Benjamin";
    priimek= "Sloncek";
    systolic= 175;
    diastolic= 93;
  }

  $.ajax({
  	url: baseUrl + "/ehr",
  	type: 'POST',
  	headers: {
        "Authorization": getAuthorization()
    },
  	success: function (data) {
  	  var ehrId = data.ehrId;
  	  var partyData = {
  		  firstNames: ime,
  		  lastNames: priimek,
  		  additionalInfo: {"ehrId" : ehrId}
  	  };
  	    //console.log(JSON.stringify(partyData));
  		  $.ajax({
          url: baseUrl + "/demographics/party",
          type: 'POST',
          headers: {
            "Authorization": getAuthorization()
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              console.log(ehrId);
              document.getElementById(ime).value = ehrId;
  			      $("#noviId").append("<div class='panel panel-default' id='novo'><div class='panel-body'><span>Kreiran testni pacient<b> "+ime+
  			      " "+priimek+"</b> z ehrId-jem <b>" + ehrId + "</b>.</br></span></div></div>");
  			      funkcija(systolic, diastolic, ehrId);
            }
          },
          error: function(err){
            console.log(JSON.parse(err.responseText).userMessage);
          }
  });
  return ehrId;
}});
}

function funkcija(systolic, diastolic, ehrId){
    var podatki = {
	      "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": null,
		    "vital_signs/height_length/any_event/body_height_length": null,
		    "vital_signs/body_weight/any_event/body_weight": null,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": null,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": systolic,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolic,
        "vital_signs/indirect_oximetry:0/spo2|numerator": null
	};


	var parametriZahteve = {
	    "ehrId": ehrId,
	    templateId: 'Vital Signs',
	    format: 'FLAT',
	    committer: 'Nekdo'
	};
	$.ajax({
	    url: baseUrl + "/composition?" + $.param(parametriZahteve),
	    type: 'POST',
	    contentType: 'application/json',
	    data: JSON.stringify(podatki),
	     headers: {
        "Authorization": getAuthorization()
      },
	    success: function (res) {
	    	console.log(res.meta.href);
	    },
	    error: function(err){
	      console.log(JSON.parse(err.responseText).userMessage);
	     },
  });
  if(stPacienta<=3){
    generirajPodatke(stPacienta++);
  }
}	




function preberiPodatkeOBolniku() {
  var ehrId = document.getElementById("preberiEHRid").value;
  //console.log(ehrId);
  $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
        "Authorization": getAuthorization()
      },
    	success: function (data) {
    	  document.getElementById("panel").className= "panel" + " " + "panel-default";
  			var party = data.party;
  			$("#haha").remove();
  			$("#result1").append("<div id='haha'><span><b>Ime:</b> " + party.firstNames + "</br><b>Priimek:</b> " +
          party.lastNames + "</span></div>");
  		}
  });
  
  $.ajax({
      url: baseUrl + "/view/" + ehrId + "/blood_pressure",
      type: 'GET',
      headers: {
          "Authorization": getAuthorization()
      },
      success: function (res) {
          console.log(res);
          $("#nekId").remove();
          $("#result2").append("<div id='nekId'><span><b>Sistolični krvni tlak:</b> "+ 
          res[res.length-1].systolic +" "+ res[res.length-1].unit + "</br>" +
          "<b>Diastolični krvni tlak:</b> "+ res[res.length-1].diastolic +" "+ res[res.length-1].unit + "</br></span>");
          generirajKomentar(res[res.length-1].systolic, res[res.length-1].diastolic);
      }
  });
  return ehrId;
}

function generirajKomentar(sistolični, diastolični){
  $("#to").remove();
  if(180<sistolični || 110<diastolični){
   $("#komentar").append("<div class='panel-body' id='to'><span>Oseba ima <b>hipertenzivno krizo</b> (močno povišan krvni tlak).<br></span></div>");
   return; 
  }
  if((160<sistolični && sistolični<180)  || (100<diastolični && diastolični<110)){
    $("#komentar").append("<div class='panel-body' id='to'><span>Oseba ima <b>hipertenzijo 2. stopnje</b> (povišan krvni tlak).<br></span></div>");
    return;
  }
  if((140<sistolični && sistolični<160) || (90<diastolični && diastolični<100)){
    $("#komentar").append("<div class='panel-body' id='to'><span>Oseba ima <b>hipertenzijo 1. stopnje</b> (povišan krvni tlak).<br></span></div>");
    return;
  }
  if((120<sistolični && sistolični<140) || (80<diastolični && diastolični<90)){
    $("#komentar").append("<div class='panel-body' id='to'><span>Oseba ima <b>predhipertenzijo</b> (povišan krvni tlak).<br></span></div>");
    return;
  }
  if((90<sistolični && sistolični<120) || (60<diastolični && diastolični<80)){
    $("#komentar").append("<div class='panel-body' id='to'><span>Oseba ima <b>idealen</b> krvni tlak.<br></span></div>");
    return;
  }
  else{
    $("#komentar").append("<div class='panel-body' id='to'><span>Oseba ima <b>hipotenzijo</b> (nizek krvni tlak).<br></span></div>");
    return;
  }
}

window.addEventListener('load', function() {
  $('#testniEHR').change(function() {
  		$("#preberiEHRid").val($(this).val());
  })
});